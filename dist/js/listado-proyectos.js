$(document).ready(function () {
    $('body').on('click', '.Ver-mas', function () {
        loader();
        $.post('dist/libs/ac_listado', {
            'proyecto[action]': 'Proyecto-info',
            'proyecto[proyecto]': $(this).attr('data-proyecto'),
        }, function (data) {
            $('#fondo').remove();
            $.dialog({
                title: '',
                content: '<div class="Conten-modal-completo">' +
                    '<div class="Conten-modal-completo-sec">' +
                    '<h4>' + data.info[0].nombre + '</h4>' +
                    data.info[0].descripcion +
                    '</div>' +
                    '</div>',
                boxWidth: '80%',
                useBootstrap: false,
                draggable: false,
                animation: 'bottom',
                closeAnimation: 'opacity',
                animateFromElement: false
            });
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    AOS.init();
    $(function () {
        $('#slides').superslides({
            inherit_width_from: '.wide-container',
            inherit_height_from: '.wide-container',
            play: 3000,
            animation: 'fade'
        });
    });

    var scroll = new SmoothScroll('a[href*="#"]', {

        // Selectors
        ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
        header: null, // Selector for fixed headers (must be a valid CSS selector)
        topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

        // Speed & Duration
        speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
        speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
        durationMax: null, // Integer. The maximum amount of time the scroll animation should take
        durationMin: null, // Integer. The minimum amount of time the scroll animation should take
        clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
        offset: 80,


        easing: 'easeInOutCubic', // Easing pattern to use
        customEasing: function (time) {


            return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

        },

        // History
        updateURL: true, // Update the URL on scroll
        popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

        // Custom Events
        emitEvents: true // Emit custom events

    });

});
