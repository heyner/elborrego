$(document).ready(function () {
  $("select").formSelect();
  $(".modal").modal();
  $(".datepicker").datepicker();
  $('.collapsible').collapsible();
  $('.tooltipped').tooltip();
  $('input#input_text, textarea#Descripcion-riesgo').characterCounter();

});
