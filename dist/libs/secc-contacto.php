<div class="Contenedor-global">
  <div class="Contenedor-global-int Texto-centro">
    <h2 class="Mayus">Contáctenos</h2>
    <div class="Conten-formulario">
      <form id="Form-contacto" class="Forms-borre">
        <div class="Conten-bloque">
          <div class="Conten2">
            <input type="text" placeholder="Nombre" name="contacto[nombre]" required>
          </div>
          <div class="Conten2">
            <input type="text" placeholder="Teléfono" name="contacto[telefono]" required>
          </div>
        </div>
        <div class="Conten-bloque">
          <div class="Conten2">
            <input type="email" placeholder="Correo electrónico" name="contacto[correo]">
          </div>
          <div class="Conten2">
            <input type="text" placeholder="Empresa" name="contacto[empresa]">
          </div>
        </div>
        <div class="Conten-bloque">
          <div class="Conten1">
            <textarea name="contacto[mensaje]" id="" placeholder="Mensaje"></textarea>
          </div>
        </div>
        <div class="Conten-bloque">
          <div class="Conten1 Texto-izq">
            <input type="submit" class="Btn-verde" value="ENVIAR MENSAJE">
          </div>
        </div>
      </form>
      <p>
        <br>
      <ul class="Info-contacto">
        <li><i class="icon-envelop"> </i> Correo electrónico: certificaciones@elborrego.co</li>
        <li><i class="icon-phone"> </i> Teléfono: 315 526 01 77 o 3188271416</li>
        <li>Para radicar - Peticiones, Quejas, Reconocimientos y Sugerencias.</li>
      </ul>
      </p>
      <p class="Texto-izq"><a href="javascript:void(0)" id="Op-modal" class="Btn-verde">Clic Aquí</a> </p>

    </div>

  </div>
</div>

<div class="Modal-pq Modal-oculta">
  <div class="Modal-pq-int">
    <div class="Modal-pq-int-conten">
      <div class="Modal-pq-int-contexto">
        <div class="Modal-pq-cerrar">
          <a href="javascript:void(0)" id="Cer-modal"><span><i class="icon-cross"></i></span></a>
        </div>
        <div class="Modal-pq-contenido">
          <div class="Modal-pq-formulario">
            <h2 class="Titulo-h2 Mayus Texto-centro Center">PQRS</h2>
            <form id="Form-pqrs" class="Forms-borre">
              <div class="Conten-bloque">
                <div class="Conten2">
                  <select name="contacto[tipo]" id="" required>
                    <option value=""> Seleccionar</option>
                    <option value="Petición"> Petición</option>
                    <option value="Quejas"> Quejas</option>
                    <option value="Reconocimientos"> Reconocimientos</option>
                    <option value="Sugerencias"> Sugerencias</option>
                  </select>
                </div>
                <div class="Conten2">
                  <input type="text" placeholder="Nombre" name="contacto[nombre]" required>
                </div>
              </div>
              <div class="Conten-bloque">
                <div class="Conten2">
                  <input type="email" placeholder="Correo electrónico" name="contacto[correo]" required>
                </div>
                <div class="Conten2">
                  <input type="text" placeholder="Teléfono" name="contacto[telefono]" required>
                </div>
              </div>
              <div class="Conten-bloque">
                <div class="Conten1">
                  <textarea name="contacto[mensaje]" id="" placeholder="Mensaje"></textarea>
                </div>
              </div>
              <p>Como quieres que te respondamos tu solicitud:</p>
              <div class="Conten-bloque">
                <div class="Conten1">
                  <label>
                    <input type="checkbox" name="contacto[respuesta]" id="cbox1" value="Correo" required> Correo electrónico
                  </label>
                  <label>
                    <input type="checkbox" name="contacto[respuesta]" id="cbox2" value="Teléfono"> Teléfono
                  </label>
                  <label>
                    <input type="checkbox" name="contacto[respuesta]" id="cbox2" value="Dirección"> Dirección en físico
                  </label>
                </div>
              </div>
              <div class="Conten-bloque">
                <div class="Conten1 Texto-izq">
                  <input type="submit" class="Btn-verde" value="ENVIAR SOLICITUD">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
