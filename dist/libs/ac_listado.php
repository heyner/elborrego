<?php
require '../../dist/libs/conexion.php';

$data   = $_REQUEST['proyecto'];
$msg    = [];

switch ($data['action']) {
    case 'Proyecto-info':
        $idproyecto = explode('-', $data['proyecto']);

        $check = $db
            ->where('Id_p', $idproyecto[1])
            ->objectBuilder()->get('proyectos');

        if ($db->count > 0) {
            $proyectos = $db
                ->where('Id_p', $idproyecto[1])
                ->objectBuilder()->get('proyectos', null, ['nombre_p AS nombre, descripcion_p AS descripcion, imagen_p AS imagen']);

            $msg['status'] = true;
            $msg['info']   = $proyectos;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el proyecto no existe!';
        }

        echo json_encode($msg);
        break;
}
