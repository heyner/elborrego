<?php
require_once "conexion.php";

$data   = $_REQUEST['contacto'];
$msg    = [];

switch ($data['opc']) {
    case 'Mensaje-contacto':
        $cabeceras = 'From: noreply@elborrego.com';
        $asunto    = "Mensaje de Contacto";
        $email_to  = 'certificaciones@elborrego.co';
        $contenido = "Datos de Contacto\n"
            . "\n"
            . "Nombre: $data[nombre]\n"
            . "Teléfono: $data[telefono]\n"
            . "Correo electrónico: $data[correo]\n"
            . "Emprea: $data[empresa]\n"
            . "Mensaje: $data[mensaje]\n";

        if (mail($email_to, $asunto, $contenido, $cabeceras)) {
            $msg['status']   = true;
            $msg['msg'] = 'Tu mensaje ha sido enviado!';
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, tu mensaje no pudo ser enviado';
        }

        break;

    case 'Mensaje-PQRS':
        $cabeceras = 'From: noreply@elborrego.com';
        $asunto    = "Mensaje de PQRS";
        $email_to  = 'certificaciones@elborrego.co';
        $contenido = "Datos de Contacto\n"
            . "\n"
            . "Tipo: $data[tipo]\n"
            . "Nombre: $data[nombre]\n"
            . "Teléfono: $data[telefono]\n"
            . "Correo electrónico: $data[correo]\n"
            . "Quiere que se le responda por mmedio de: $data[respuesta]\n"
            . "Mensaje: $data[mensaje]\n";

        if (mail($email_to, $asunto, $contenido, $cabeceras)) {
            $msg['status']   = true;
            $msg['msg'] = 'Tu mensaje ha sido enviado!';
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, tu mensaje no pudo ser enviado';
        }
        break;
}

echo json_encode($msg);
