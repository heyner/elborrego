<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="All">
  <title>Inversiones el Borrego</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
</head>

<body>
  <header>
    <?php include('dist/libs/header-top.php') ?>
  </header>
  <section class="Marg-top">
    <div class="Conten-acerca">
      <div class="Conten-acerca-int">
        <div class="Conten-acerca-int-sec Acerca-bag-img">
        </div>
        <div class="Conten-acerca-int-sec Acerca-context">
          <div class="Acerca-contenido">
            <h2 class="Titulo-h2 Mayus">Misión</h2>
            <p>Inversiones el borrego s.a.s. es una empresa privada dedicada a la producción de racimo de fruta fresca de palma pionero en tecnología de punta, en la siembra y cultivo, bajo lineamientos de un manejo agroecológico, obteniendo como resultado un fruto de excelente calidad, como un resultado humano comprometido que fundamenta sus actuaciones en responsabilidades, respeto por el medio ambiente, contribuyendo al desarrollo sostenible de la comunidad</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="">
    <div class="Conten-acerca">
      <div class="Conten-acerca-int">
        <div class="Conten-acerca-int-sec Acerca-context Bg-naranja">
          <div class="Acerca-contenido" data-aos="fade-right" data-aos-delay="580" data-aos-duration="1500">
            <h2 class=" Titulo-h2 Mayus">Visión</h2>
            <p>Para el 2023 inversiones el borrego s.a.s. espera seguir siendo uno de los mejores productores de racimo de fruta fresca de palma de aceite de la región palmera, a través de la adopción de tecnología de punta en cultivos de palma de aceite, con estrategias claras, definidas, de permanente mejoramiento y una clara orientación hacia el progreso del recurso humano, el cual se distinga por su compromiso y su sentido de pertenencia con la organización.</p>
          </div>
        </div>
        <div class="Conten-acerca-int-sec Acerca-bag-img" style="background-image: url(dist/assets/images/imagen_4.jpeg);">

        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="Contenedor-global">
      <div class="Contenedor-global-int Texto-centro">
        <h2 class="Mayus">Políticas de la organización</h2>
      </div>
    </div>

    <div class="Conten-acerca">
      <div class="Conten-acerca-int">
        <div class="Conten-acerca-int-sec Acerca-bag-img" style="background-image: url(dist/assets/images/imagen_5.jpeg);">
        </div>
        <div class="Conten-acerca-int-sec Acerca-context Bg-verde">
          <div class="Acerca-contenido" data-aos="fade-left" data-aos-delay="580" data-aos-duration="1500">
            <h2 class="Titulo-h2 Mayus Text-blanco">RESPETO DE LOS DERECHOS HUMANOS
            </h2>
            <p class="Text-blanco">
              Se compromete a respetar los Derechos Humanos en todas sus operaciones con base a la legislación aplicable y La Declaración Universal de Los Derechos Humanos que contempla:
            <ul class="Text-blanco">
              <li>Respetar y acatar las leyes Colombianas.</li>
              <li>Garantizar la seguridad de nuestros empleados, sujeto a las leyes y respeto por los Derechos Humanos.</li>
              <li>Respetar los derechos y libertades de trabajadores, miembros de La Empresa, contratistas que presten servicios a la operación y de las comunidades en el área de influencia sin discriminación alguna; actuar con debida diligencia y no ser cómplices de violaciones a los derechos humanos.</li>
              <li>Promover los derechos humanos en toda nuestra área de influencia.</li>
              <li>Prestar atención a los derechos de niños, niñas, mujeres, discapacitados, adultos mayores, grupos étnicos, minorías y desplazados.</li>
              <!-- <li>Respetar costumbres y cultura de las comunidades vecinas y propender por una adecuada relación con todos sus miembros.
                Dialogar respetuosamente con todos los grupos interesados en hacerlo.</li> -->
              <li>Implementar esta política comprometiendo a todos los niveles de la empresa, desarrolle y aplique indicadores que permitan su seguimiento, el mejoramiento continuo.</li>
              <li>Identificar, prevenir, mitigar, remediar y/o compensar en todos los casos los eventuales impactos sobre los derechos de las comunidades vecinas y empleados.</li>
            </ul>

            </p>
          </div>
        </div>
      </div>
    </div>


    <div class="Conten-acerca">
      <div class="Conten-acerca-int">
        <div class="Conten-acerca-int-sec Acerca-context">
          <div class="Acerca-contenido" data-aos="fade-right" data-aos-delay="580" data-aos-duration="1500">
            <h2 class="Titulo-h2 Mayus">PREVENCIÓN DEL ACOSO SEXUAL Y LABORAL</h2>
            <p>Toda persona tiene derecho a que se le respete su dignidad y a verse libre de toda forma de acoso sexual y/o laboral en su lugar de trabajo, por tanto no tolera ni acepta conductas no deseadas ni impropias entre sus colaboradores, da prioridad a la prevención y eliminación de acoso sexual y/o laboral para evitar afectación en el bienestar físico y psicológico de los empleados.
            </p>
            <h2 class="Titulo-h2 Mayus">CONSERVACIÓN DE LA FAUNA</h2>
            <p>Se considera una falta grave el capturar, dañar o matar la fauna existente en especial las raras, amenazadas o en peligro de extinción, en la instalaciones de la empresa y donde esta opere.
            </p>
            <h2 class="Titulo-h2 Mayus">PROHIBICIÓN DEL USO DEL FUEGO</h2>
            <p>Se prohíbe el uso del fuego para las labores de preparación de tierras y demás actividades realizadas dentro de la organización. En caso de requerirse se debe contar con la debida autorización de la autoridad competente.</p>
          </div>
        </div>
        <div class="Conten-acerca-int-sec Acerca-bag-img" style="background-image: url(dist/assets/images/imagen_6.jpeg);">

        </div>
      </div>
    </div>

  </section>

  <section>
    <div class="Contenedor-global">
      <div class="Contenedor-global-int Texto-centro">
        <h2 class="Mayus">RESEÑA HISTÓRICA INVERSIONES EL BORREGO S.AS</h2>

        <p class="Text-justificado Texto-parrafo">Luis Francisco Barreto Solano estuvo vinculado a la palma africana desde que comenzó su cultivo en la zona Oriental. Como funcionario del IFA dirigió procesos de colonización, promovió el cultivo de la palma africana y brindo asesoría técnica a muchos de los primeros palmicultores del llano. Desde esa época abrigo la convicción sobre las bondades y el futuro de la Elaeis guineensis; pero carecía de los recursos para iniciar su propia plantación de la manera como consideraba debía tenerla: con todas las de la ley, en suelos adecuados, con buen material de siembra y prácticas ajustadas a las necesidades del cultivo.
        </p>
        <p class="Text-justificado Texto-parrafo">Al retirase del IFA se mantuvo atento a los desarrollos de la palma, pero se dedicó de lleno al cultivo de arroz. Incluso “colonizo” tierras para ese cultivo, como se explicó en páginas anteriores. En 1981 compro EL BORREGO, hacienda arrocera de la familia Vallejo Valencia que había pasado a manos del Banco del Comercio. Al adquirirla pensó que era una tierra apta para sembrar palma africana. Para entonces Barreto ya comenzaba a dudar sobre su permanencia en la actividad arrocera, pues la presidencia de la Junta Directiva de la Federación de Arroceros le habían brindado una clara percepción sobre la realidad de ese sector: el mercado del cereal se había tornado inestable y cada vez resultaba más difícil lograr acuerdos entre los productores para asegurar la comercialización del producto.
        </p>
        <p class="Text-justificado Texto-parrafo">En 1985 Barreto a un día del campo del ICA donde coincidió con Eliseo Restrepo. Le comento haber visto el cultivo de palma africana que estaba iniciando en Manavire. Restrepo, pues ya tenía planeada la construcción de su palma extractora y necesitaba proveedores. En ese momento Barreto decidió no postergar más el proyecto que había acariciado por tanto tiempo. Eso sí: asegura que si no hubiera existido la posibilidad de acceder a los créditos blandos del Fondo Financiero Agropecuario, tal vez no se hubiera decidido a emprender el cultivo de la palma.
        </p>
        <p class="Text-justificado Texto-parrafo">Al analizar los materiales disponibles, Barreto consideró que el Papua de Murgas E Lowe la mayor garantía. Con el sembró 200 hectáreas en 1986. José Vidal Vargas, su vecino y su socio en un molino de arroz, cuando advirtió que Barreto organizaba sus viveros de palma, también se interesó en el negocio e inicio su propia plantación.</p>
        <p class="Text-justificado Texto-parrafo">
          En 1987 El Borrego incremento su cultivo a 400 hectáreas y para ello empleo material Unilever de Camerun. En 1989 y 1990 hizo otras dos siembras, de 100 hectáreas cada una, con semillas elite y superelite de Unipalma. En las siembras iniciales lo apoyaron los agrónomos de Manavire: primero Silvio Benavides y después Fabio Calvo. Tiempo después vinculo de manera permanente a Oscar Mario Bastidas, agrónomo graduado en la Universidad Nacional de Medellín, como el mismo.
        </p>
        <p class="Text-justificado Texto-parrafo">
          A mediados de los noventas Barreto considero la posibilidad de montar su planta extractora, pero echo números y tras negociar con sus vecinos de yaguarito, logro que le reconocieran un buen índice de extracción. Entonces resolvió posponer indefinidamente el proyecto industrial; pertenece al grupo de quienes consideran que la actividad del productor puede llegar a ser la más rentable en la cadena de producción, siempre y cuando logre un precio justo por su fruto.
        </p>
        <p class="Text-justificado Texto-parrafo">
          Algunos expertos consideran que el Borrego es de las plantaciones que está llamada a dejar huella. Su gestor se ha esmerado en emplear buenas semillas, lo cual se refleja en la productividad: el promedio está sobre las 28 toneladas por hectárea. Además, considera Barreto la rusticidad de la línea Unilever continúa siendo superior a la de otros materiales. Este hecho, sumado al manejo racional de riesgos y drenajes, ayuda a explicar la poca incidencia de problemas fitosanitarios, proporcionalmente, una de las más bajas en la región.

        </p>
        <p class="Text-justificado Texto-parrafo">
          En términos de su manejo, la empresa se caracteriza por una organización sencilla, con una estructura administrativa reducida, y por la formación de un grupo humano que han desarrollado mística por el trabajo, sentido de pertenencia y deseo de progreso.

        </p>
        <p class="Text-justificado Texto-parrafo">
          Actualmente en 2020 Inversiones el Borrego es una encuentra en proceso de certificación por la Mesa Redonda de Palma sostenible RSPO.
        </p>
      </div>
    </div>
  </section>

  <section id="servicios">
    <?php include('dist/libs/secc-servicios.php') ?>
  </section>

  <section>
    <?php include('dist/libs/secc-proyectos.php') ?>
  </section>

  <section id="contacto">
    <?php include('dist/libs/secc-contacto.php') ?>
  </section>

  <section>
    <div class="Conten-mapa">
      <div id="map">
      </div>
    </div>
  </section>

  <footer>
    <div class="Conten-bottom">
      <p>Copyright © Inversiones el Borrego, 2021. All rights reserved Designed by <a href="https://inngeniate.com" target="_blank"> Inngeniate.com</a></p>
    </div>
  </footer>
  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script>
    var scroll = new SmoothScroll('a[href*="#"]', {

      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)
      topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

      // Speed & Duration
      speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
      speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
      durationMax: null, // Integer. The maximum amount of time the scroll animation should take
      durationMin: null, // Integer. The minimum amount of time the scroll animation should take
      clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
      offset: 80,


      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function(time) {


        return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

      },

      // History
      updateURL: true, // Update the URL on scroll
      popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

      // Custom Events
      emitEvents: true // Emit custom events

    });
  </script>
  <script>
    AOS.init();
    $(function() {
      $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        play: 3000,
        animation: 'fade'
      });
    });
  </script>

</body>

</html>