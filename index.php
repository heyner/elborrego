<?php
require 'dist/libs/conexion.php';

$ls_proyectos = '';

$proyectos = $db
  ->where('estado_p', 1)
  ->objectBuilder()->get('proyectos');

if ($db->count > 0) {
  $fila = 0;
  foreach ($proyectos as $proyecto) {
    if ($fila == 0) {
      $ls_proyectos .= '<div class="Conten-proyectos-int-contenedores">';
    }

    $ls_proyectos .= '<div class="Conten-proyectos-seccion" data-aos="zoom-in">
                        <a href="#!" class="Proact">
                          <div class="Conten-proyectos-seccion-superior">
                            <div class="Conten-proyectos-seccion-imagen">
                              <img src="' . $proyecto->imagen_p . '" alt="">
                            </div>
                            <span class="Nombre-proyecto-card">' . $proyecto->nombre_p . '</span>
                          </div>
                          <div class="Conten-proyectos-seccion-inferior">
                            <a href="#!" class="Ver-mas" data-proyecto="Proyecto-' . $proyecto->Id_p . '">SABER MÁS</a>
                          </div>
                        </a>
                      </div>';

    if ($fila == 2) {
      $ls_proyectos .= '</div>';
      $fila = 0;
    } else {
      $fila++;
    }
  }
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Inversiones el Borrego</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
</head>

<body>
  <header>
    <?php include('dist/libs/header-top.php') ?>
  </header>
  <section>
    <div class="Contenedor-slide">
      <div class="Contenedor-slide-int">
        <div class="Slider">
          <div class="Slider-imagen">
            <div class="wide-container">
              <div id="slides">
                <ul class="slides-container">
                  <li>
                    <img src="dist/assets/images/slide_1.jpeg" alt="">
                    <div class="context">
                    </div>
                  </li>
                  <li>
                    <img src="dist/assets/images/slide_2.jpeg" alt="">
                    <div class="context">
                    </div>
                  </li>
                  <li>
                    <img src="dist/assets/images/slide_3.jpeg" alt="">
                    <div class="context">
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="Slide-home">
            <div class="Slide-home-int">
              <div class="Texto-principal-slide">
                <div class="Texto-slide">
                  <div class="Texto-slide-sec aos-init aos-animate" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
                    <h2 class="Texto-h2-slide">INVERSIONES EL BORREGO</h2>
                    <h3 class="Texto-h3-slide">Somos una empresa dedicada a la producción de Palma de Aceite Sostenible</h3>
                  </div>
                  <div class="Texto-slide-sec">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="Contenedor-global">
      <div class="Contenedor-global-int Texto-centro aos-init aos-animate" data-aos="fade-down" data-aos-delay="580" data-aos-duration="1500">
        <h2 class="Titulo-h2 Mayus">Inversiones <span class="Text-Naran">el Borrego</span></h2>
        <p class="Texto-parrafo">Es una empresa privada dedicada a la producción de racimo de fruta fresca de palma pionero en tecnología de punta, en la siembra y cultivo, bajo lineamientos de un manejo agroecológico, obteniendo como resultado un fruto de excelente calidad, como un resultado humano comprometido que fundamenta sus actuaciones en responsabilidades, respeto por el medio ambiente, contribuyendo al desarrollo sostenible de la comunidad</p>

      </div>
    </div>
  </section>

  <section id="servicios">
    <?php include('dist/libs/secc-servicios.php') ?>
  </section>

  <section>
    <?php include('dist/libs/secc-proyectos.php') ?>
  </section>

  <section id="contacto">
    <?php include('dist/libs/secc-contacto.php') ?>
  </section>

  <section>
    <div class="Conten-mapa">
      <div id="map">
      </div>
    </div>
  </section>
  <footer>
    <div class="Conten-bottom">
      <p>Copyright © Inversiones el Borrego, 2021. All rights reserved Designed by <a href="https://inngeniate.com" target="_blank"> Inngeniate.com</a></p>
    </div>
  </footer>
  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/jquery-confirm.min.js"></script>
  <script src="dist/js/listado-proyectos.js?v<?php echo date('YmdHis') ?>"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>

</body>

</html>
